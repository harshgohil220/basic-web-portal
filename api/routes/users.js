var express = require('express');
var bodyParser = require("body-parser");
let jwt = require('jsonwebtoken');
var fs = require("fs");
var request = require('request');

const XLSX = require('xlsx');
const workbook = XLSX.readFile(__dirname +'/ExcelODBCUDC.xlsx');
var cors = require('cors');
const sheet_name_list = workbook.SheetNames;
var ExcelJson = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);


var router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));

var key =  {
    secretKey: "Users",
    algorithm: 'HS256',
};

router.post('/', function(req, res, next) {
    let userdata = {
        username: req.body.name,
        password: req.body.password
    };
    
    // var key =  {
    //     secretKey: userdata.username,
    //     algorithm: 'HS256',
    // };
    var flag = false;
	ExcelJson.find((_obj)=>{
		if(_obj.userid === userdata.username && _obj.Password === userdata.password){
			flag = true;
		}
	});
    if (flag === true) {
        console.log(flag);
        let token = jwt.sign(userdata, key.secretKey, {
            algorithm: key.algorithm,
            expiresIn: '5m'
        });
        console.log(userdata.username);
        var r = request.defaults({
            rejectUnauthorized: false,
            host: "192.168.1.151",
            ca: fs.readFileSync(__dirname+'\\Certificates\\root.pem'), 
            cert: fs.readFileSync( __dirname+'\\Certificates\\client.pem'),
            key: fs.readFileSync(__dirname+'\\Certificates\\client_key.pem'),
            passphrase: ''
        });
        var b = JSON.stringify({
            "UserDirectory": "WIN-MIBG6S1J3R3", 
            "UserId": userdata.username, 
            "Attributes": []
        });
        r.post({
            url: "https://192.168.1.151:4243/qps/ticket?xrfkey=abcdefghijklmnop",
            headers: {
                'rejectUnauthorized':false, 
                'x-qlik-xrfkey': 'abcdefghijklmnop',
                'content-type': 'application/json',
                'x-qlik-user': 'UserDirectory=WIN-MIBG6S1J3R3; UserId='+userdata.username 
            },body:b
        },function(err, respo, body) {
            console.log(body);
            if(JSON.parse(body)['Ticket']){
                var ticket = JSON.parse(body)['Ticket'];
                res.status(200).json({
                    message: true,
                    jwtoken: token,
                    ticket: "?qlikTicket="+ticket
                });
                // let user = { 
                //     ticket: "?qlikTicket="+ticket
                // }; 
                // var dictstring = JSON.stringify({"ticket":'?qlikTicket='+ticket});
            }else{
                console.log(error);
            }
        });
    }
  else {
      res.status(401).json({
          message: false
      });
  }
});

module.exports = router;
