var express = require('express');
var bodyParser = require("body-parser");
let jwt = require('jsonwebtoken');



var router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));

var key =  {
    secretKey: "Users",
    algorithm: 'HS256',
};

router.post('/', function(req, res, next) {
    jwt.verify(req.body.token, key.secretKey,{
         algorithm: key.algorithm       
    }, function (err, decoded) {
        if (err) {
            let errordata = {
                message: err.message,
                expiredAt: err.expiredAt
            };
            return res.status(401).json({
                message: 'Unauthorized Access'
            });
        }else{
            return res.status(200).json({
                message: decoded.username,
                ticket: req.body.ticket
            })
        }
    })
});

module.exports = router;
