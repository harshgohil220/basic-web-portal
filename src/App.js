import React from "react";
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import Login from "./Compnent/Login";

class App extends React.Component{
  state={ 
    isLog:false
  }

  handleLogin=(isLog) => this.setState({isLog})

  render(){     
    const {isLog} = this.state;

    if(isLog === true){      
      return <Redirect to='/dashboard' />
    } 

    return (
      <div className="App">
        <BrowserRouter>
          <Switch>
            <Route exact path="/" render={() =><Login isLogin={this.handleLogin}/>} />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
