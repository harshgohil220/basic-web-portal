import React from "react";
import "../Compnent/Login.css";
import "./assets/css/shared/style.css";
import "./assets/css/demo_1/style.css";

export default class Home extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            url:''
        };
    }
    componentDidMount() {  
        const that = this;
        const storage = sessionStorage.getItem('token');
        const ticket = sessionStorage.getItem('ticket');
        fetch('/dashboard',{
            method: 'POST',
            body: JSON.stringify({token:storage,ticket:ticket}),
            headers: {"Content-Type": "application/json"}
        })
        .then(function(response){
            return response.json()
        }).then(function(body){
            that.setState({
                url: "https://192.168.1.151/hub"+body.ticket,
                user: body.message
            }); 
            console.log(body);
        }); 
        console.log(that);
    }
    render(){
        return(
            // <div>
            //     <iframe className="iframe" src={this.state.url}></iframe>
            // </div>
            <div className="container-scroller">
                {/* <nav className="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
                    <div className="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
                    </div>
                </nav> */}
                
                <div className="container-fluid page-body-wrapper">
                    <nav className="sidebar sidebar-offcanvas" id="sidebar">
                    <ul className="nav">
                        <li className="nav-item nav-category" style={{marginTop :'15px'}}>Hello {this.state.user}</li>
                        <li className="nav-item">
                            <a className="nav-link" href="http://localhost:3000/dashboard">
                                <i className="menu-icon typcn typcn-document-text"></i>
                                <span className="menu-title">Dashboard</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="http://localhost:3000/">
                                <i class="menu-icon typcn typcn-document-text"></i>
                                <span class="menu-title">Logout</span>
                            </a>
                        </li>
                    </ul>
                    </nav>
                    <div className="main-panel">
                         <iframe className="iframe" src={this.state.url}></iframe>
                    </div>
                </div>
            </div>
        )
    }
}