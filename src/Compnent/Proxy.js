const proxy = require('http-proxy-middleware');

module.exports = function(app) {

  app.use(proxy('/login', {
    target: 'http://localhost:8080',
    pathRewrite: {'^/login' : ''}
  }));

  app.use(proxy('/dashoboard', {
    target: 'http://localhost:8080',
    pathRewrite: {'^/dashoboard' : ''}
  }));
  
};