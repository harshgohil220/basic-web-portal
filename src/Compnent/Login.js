import React from "react";
import "./Login.css";

export default class Login extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            name:'',
            password:'',
        };

    }

    handleChange = async  (e) => {
        const {name,value} = e.target;
        this.setState({[name]:value})
    }

    handleSubmit = async (e) => {  
        e.preventDefault(); 
        let errors = {};
        const that = this;
        const user_details = that.state;
        fetch('/users',{
            method: 'POST',
            body: JSON.stringify(user_details),
            headers: {"Content-Type": "application/json"}
        })
        .then(function(response){
            return response.json()
        }).then(function(body){
            if(body.message === true){
                console.log(body);  
                sessionStorage.setItem('token', body.jwtoken);
                sessionStorage.setItem('ticket',body.ticket);         
                that.props.isLogin(true);
            }else{
                errors="Please enter correct input";
                that.setState({
                    errors: errors
                }); 
            }
        });       
    }
    render(){
        return(
            <div className="div-login">
                <form onSubmit={this.handleSubmit}>
                    <h1>Web Portal</h1>
                    <input type="name" name="name" placeholder="Enter UserName" required onChange={this.handleChange}/>
                    <input type="password" name="password" placeholder="Enter Password" required onChange={this.handleChange}/>
                    <div className="text-danger">{this.state.errors}</div>
                    <button onSubmit={this.handleSubmit}>Login</button>
                </form>
            </div>
        )
    }
}