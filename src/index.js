import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import Home from './Home/Home';
 
ReactDOM.render(
  <Router>
    <Switch>
      {/* <Route exact path="/" component={App}/>
      <Route path="/home" component={Home}/> */}
      <Route exact={true} path={'/'} component={App}/>
      <Route exact={true} path={'/dashboard'} component={Home}/>
    </Switch>
  </Router>,
  document.getElementById('root')
);

reportWebVitals();